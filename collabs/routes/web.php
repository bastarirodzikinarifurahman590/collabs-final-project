<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('body');
});
Route::get('/team','teamController@index')->name('post.index');
Route::get('/team/create','teamController@create');
Route::post('/team','teamController@store');
Route::get('/team/{id}','teamController@show');
Route::get('/team/{id}/edit','teamController@edit');
Route::put('/team/{id}','teamController@update');
Route::delete('/team/{id}','teamController@destroy');


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');



Route::get('/servicess','serviceController@index');

Route::resource('portofolio','PortofolioController');
Route::get('/Portofolio','PortofolioController@index');



Route::get('/company','companyController@index');

Route::get('/test-dompdf',function(){
    $pdf = App::make('dompdf.wrapper');
    $pdf->loadHTML('<h1>Test</h1>');
    return $pdf->stream();
});


Route::get('test-excel', 'UserController@export');