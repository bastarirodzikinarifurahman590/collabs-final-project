<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\team;
use DB;
use Auth;


class teamController extends Controller
{
    public function __construct(){
        $this->middleware('auth')->only(['create','edit','update','store']);
    }
    public function create(){
        return view('team.create');
    }
    public function store(Request $request){
        $request->validate([
            'nama' => 'required|unique:team',
            'keahlian' => 'required',
            'gender' => 'required',
        ]);
       // dd($request->all());
   // $query = DB::table('team')->insert([
      // "nama" => $request["judul"],
       // "keahlian" => $request["keahlian"],
       // "gender" => $request["gender"]
   //]);
   
   $team = team::create([
       "nama"=> $request["nama"],
      "keahlian"=> $request["keahlian"],
      "gender"=> $request["gender"],
      "user_id"=>Auth::id()
   ]);
   return redirect('/team')->with('success', 'team Berhasil Disimpan');
 }
 public function index()
    {
        //$team = DB::table('team')->get();
     $team = team::all();
        return view('team.index', compact('team'));
}
public function show($id){
    //$post = DB::table('post')->where('id',$id)->first();
    $team = team::find($id);
    //dd($post);
    return view('team.show', compact('team'));
}
public function edit($id){
   // $post = DB::table('post')->where('id',$id)->first(); 
   $team = team::find($id);
    return view('team.edit', compact('team'));
}
public function update($id, Request $request){

//$query= DB::table('post')
      // ->where('id', $id)
      // ->update([
         //  'judul'=>$request['judul'],
          // 'isi'=>$request['isi']
      // ]);
       $update = team::where('id', $id)->update([
        "nama" => $request["nama"],
        "keahlian" => $request["keahlian"],
        "gender" => $request["gender"]
      ]);
       
       return redirect('/team')->with('success', 'Berhasil Di Update');
}
public function destroy($id){
//$query= DB::table('post')->where('id',$id)->delete();
team::destroy($id);
return redirect('/team')->with('success', 'Berhasil Dihapus');
}
}




