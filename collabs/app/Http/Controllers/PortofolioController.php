<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Portofolio;

class PortofolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //  $post = DB::table('portofolio')->get();
        // return view('portofolio.index', compact('post'));
        return view('portofolio.index');
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('portofolio.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required|unique:portofolio',
            'deskripsi' => 'required',
        ]);
        $query = DB::table('portofolio')->insert([
            "judul" => $request["judul"],
            "deskripsi" => $request["deskripsi"]
        ]);
        return redirect('/portofolio');
  
     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = DB::table('portofolio')->where('id', $id)->first();
        return view('portofolio.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = DB::table('portofolio')->where('id', $id)->first();
        return view('portofolio.edit', compact('post'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required|unique:portofolio',
            'deskripsi' => 'required',
        ]);

        $query = DB::table('portofolio')
            ->where('id', $id)
            ->update([
                'judul' => $request["judul"],
                'deskripsi' => $request["deskripsi"]
            ]);
        return redirect('/portofolio');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = DB::table('portofolio')->where('id', $id)->delete();
        return redirect('/portofolio');
    
    }
}
