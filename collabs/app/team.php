<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class team extends Model
{
    protected $table = "team";
    protected $guarded = [];
    public function user(){
        return $this->belongsTo('App\user');
    }
    public function company(){
        return $this->belongsTo('App\company');
    }
}
