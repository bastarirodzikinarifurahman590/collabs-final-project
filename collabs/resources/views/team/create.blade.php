@extends('adminlte.master')


@section('content')
    <div class="ml-3 mt-3 mr-2">
       <div  class= "card card-primary">
         <div class="card-header">
         <h2 class="card-title">Tambah Data</h2>
         </div>

        <form action="/team" method="POST">
            @csrf
            <div class="form-group mr-2 ml-2">
                <label for="nama">nama</label>
                <input type="text" class="form-control" name="nama" id="nama"  placeholder="Masukkan nama">
                @error('title')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group mr-2 ml-2">
                <label for="keahlian"> keahlian</label><br>
                <input type="text" class="form-control" name="keahlian" id="keahhlian"  placeholder="Masukkan keahlian">
                @error('body')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group mr-2 ml-2">
                <label for="gender"> gender</label><br>
                <input type="text" class="form-control" name="gender" id="gender"  placeholder="Masukkan gender">
                @error('body')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div><br><br><br>
            
            <button type="submit" class="btn btn-primary mr-2 ml-2 mb-2">Tambah</button>
        </form>
      </div>
    </div>
    @endsection