@extends('adminlte.master')


@section('content')

<div class="card-header">
<h3 align ="center"> Our Team</h3>
</div>
<div>
@if(session('success'))
<div class="alert alert-success">
{{session('success')}}
</div>
@endif
<a href="/team/create" class="btn btn-primary mt-2 mb-2 ml-2">Tambah</a>
        <table class="table table-bordered">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">nama</th>
                <th scope="col">keahlian</th>
                <th scope="col">gender</th>
                <th scope="col" style="width: 40px">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse($team as $key=>$team)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$team->nama}}</td>
                        <td>{{$team->keahlian}}</td>
                        <td>{{$team->gender}}</td>
                        <td style= display:flex;>
                            <a href="/team/{{$team->id}}" class="btn btn-info btn-sm">Show</a>
                            <a href="/team/{{$team->id}}/edit" class="btn btn-default btn-sm">edit</a>
                            <form action="/team/{{$team->id}}" method="post">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="delete" class="btn btn-danger btn-sm">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="3" align ="center">No posts</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
</div>
@endsection