@extends('adminlte.master')

@section('content')
<div class="ml-3 mt-3 mr-3">
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">PORTOFOLIO</h3>
        </div>

        <form role="form" action="/portofolio" method="POST">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" name="judul" id="judul" value="{{ old('judul', '')}}" placeholder="Masukkan Judul" required>
                    @error('judul')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="body">Deskripsi</label>
                    <textarea name="deskripsi" id="body" class="form-dontrol" cols="30" rows="10"></textarea>                    
                    {{-- <input type="text" class="form-control" name="deskripsi" id="deskripsi" value="{{ old('deskripsi', '')}}" placeholder="Masukkan Deskripsi" required> --}}
                    @error('body')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
            </div>

            <div class="card-footer">
            <button type="submit" class="btn btn-primary">Tambah</button>
            </div>
        </form>
    </div>
</div>
    
@endsection