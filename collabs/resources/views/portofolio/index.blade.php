@extends('adminlte.master')

@section('content')
<body>
  <header class="site-navbar py-4 js-sticky-header site-navbar-target" role="banner">

    <div class="container">
      <div class="row align-items-center">
        
        <div class="col-6 col-xl-2">
          <h1 class="mb-0 site-logo"><a href="index.html" class="h2 mb-0">Nitro<span class="text-primary">.</span> </a></h1>
        </div>

        <div class="col-12 col-md-10 d-none d-xl-block">
          <nav class="site-navigation position-relative text-right" role="navigation">

            <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">
              <li><a href="index.html" class="nav-link">Home</a></li>
              <li>
                <a href="about.html" class="nav-link">About Us</a>
              </li>
              
              <li><a href="portfolio.html" class="nav-link">Portfolio</a></li>
              <li><a href="tim.html" class="nav-link">our team</a></li>
              <li><a href="service.html" class="nav-link">Services</a></li>
              <li><a href="testimonal.html" class="nav-link">Testimonials</a></li>
              <li><a href="blog.html" class="nav-link">Blog</a></li>
              <li><a href="contact.html" class="nav-link">Contact</a></li>
            </ul>
          </nav>
        </div>


        <div class="col-6 d-inline-block d-xl-none ml-md-0 py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle float-right"><span class="icon-menu h3"></span></a></div>

      </div>
    </div>
    
  </header>

        <div class="container">
  
          <div class="row mb-3">
            <div class="col-12 text-center" data-aos="fade">
              <h2 class="section-title mb-3">Portfolio</h2>
            </div>
          </div>
  
          <div class="row justify-content-center mb-5" data-aos="fade-up">
            <div id="filters" class="filters text-center button-group col-md-7">
              <button class="btn btn-primary active" data-filter="*">All</button>
              <button class="btn btn-primary" data-filter=".web">Web</button>
              <button class="btn btn-primary" data-filter=".design">Design</button>
              <button class="btn btn-primary" data-filter=".brand">Brand</button>
            </div>
          </div>  
          
          <div id="posts" class="row no-gutter">
            <div class="item web col-sm-6 col-md-4 col-lg-4 col-xl-3 mb-4">
              <a href="" class="item-wrap fancybox" data-fancybox="gallery2">
                <span class="icon-search2"></span>
                <img class="img-fluid" src="{{ asset('project/images/img_1.jpg')}}">
              </a> 
    
            </div>
            <div class="item web col-sm-6 col-md-4 col-lg-4 col-xl-3 mb-4">
               <a href="" class="item-wrap fancybox" data-fancybox="gallery2">
                <span class="icon-search2"></span>
                <img class="img-fluid" src="{{ asset('project/images/img_2.jpg')}}">
              </a> 
             
            </div>
  
            <div class="item brand col-sm-6 col-md-4 col-lg-4 col-xl-3 mb-4">
               <a href="" class="item-wrap fancybox" data-fancybox="gallery2">
                <span class="icon-search2"></span>
                <img class="img-fluid" src="{{ asset('project/images/img_3.jpg')}}">
              </a> 
              
            </div>

  
            <div class="item web col-sm-6 col-md-4 col-lg-4 col-xl-3 mb-4">
               <a href="" class="item-wrap fancybox" data-fancybox="gallery2">
                <span class="icon-search2"></span>
                <img class="img-fluid" src="{{ asset('project/images/img_7.jpg')}}">
              </a> 
             
            </div>
  
            <div class="item design col-sm-6 col-md-4 col-lg-4 col-xl-3 mb-4">
               <a href="" class="item-wrap fancybox" data-fancybox="gallery2">
                <span class="icon-search2"></span>
                <img class="img-fluid" src="{{ asset('project/images/img_8.jpg')}}">
              </a> 
              
            </div>
  
            <div class="item web col-sm-6 col-md-4 col-lg-4 col-xl-3 mb-4">
               <a href="" class="item-wrap fancybox" data-fancybox="gallery2">
                <span class="icon-search2"></span>
                <img class="img-fluid" src="{{ asset('project/images/img_9.jpg')}}">
              </a> 
              
            </div>

            <div class="item brand col-sm-6 col-md-4 col-lg-4 col-xl-3 mb-4">
               <a href="" class="item-wrap fancybox" data-fancybox="gallery2">
                <span class="icon-search2"></span>
                <img class="img-fluid" src="{{ asset('project/images/img_10.jpg')}}">
              </a> 
             
            </div>

  
          </div>
        </div>
        <footer class="site-footer">
          <div class="container">
            <div class="row">
              <div class="col-md-9">
                <div class="row">
                  <div class="col-md-5">
                    <h2 class="footer-heading mb-4">About Us</h2>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Neque facere laudantium magnam voluptatum autem. Amet aliquid nesciunt veritatis aliquam.</p>
                  </div>
                  <div class="col-md-3 ml-auto">
                    <h2 class="footer-heading mb-4">Quick Links</h2>
                    <ul class="list-unstyled">
                      <li><a href="#about-section" class="smoothscroll">About Us</a></li>
                      <li><a href="#services-section" class="smoothscroll">Services</a></li>
                      <li><a href="#testimonials-section" class="smoothscroll">Testimonials</a></li>
                      <li><a href="#contact-section" class="smoothscroll">Contact Us</a></li>
                    </ul>
                  </div>
                  <div class="col-md-3">
                    <h2 class="footer-heading mb-4">Follow Us</h2>
                    <a href="#" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
                    <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
                    <a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
                    <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <h2 class="footer-heading mb-4">Subscribe Newsletter</h2>
                <form action="#" method="post" class="footer-subscribe">
                  <div class="input-group mb-3">
                    <input type="text" class="form-control border-secondary text-white bg-transparent" placeholder="Enter Email" aria-label="Enter Email" aria-describedby="button-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary text-black" type="button" id="button-addon2">Send</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="row pt-5 mt-5 text-center">
              <div class="col-md-12">
                <div class="border-top pt-5">
                  <p>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart text-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" >Colorlib</a>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                </p>
            
                </div>
              </div>
              
            </div>
          </div>
        </footer>
        <script src="{{ asset('/project/js/jquery-3.3.1.min.js')}}"></script>
        <script src="{{ asset('project/js/jquery-ui.js')}}"></script>
        <script src="{{ asset('project/js/popper.min.js')}}"></script>
        <script src="{{ asset('project/js/bootstrap.min.js')}}"></script>
        <script src="{{ asset('project/js/owl.carousel.min.js')}}"></script>
        <script src="{{ asset('project/js/jquery.countdown.min.js')}}"></script>
        <script src="{{ asset('project/js/jquery.easing.1.3.js')}}"></script>
        <script src="{{ asset('project/js/aos.js')}}"></script>
        <script src="{{ asset('project/js/jquery.fancybox.min.js')}}"></script>
        <script src="{{ asset('project/js/jquery.sticky.js')}}"></script>
        <script src="{{ asset('project/js/isotope.pkgd.min.js')}}"></script>
        <script src="{{ asset('project/js/main.js')}}"></script>
      
</body>
@endsection