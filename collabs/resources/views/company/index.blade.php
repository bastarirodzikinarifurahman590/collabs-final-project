@extends('adminlte.master')


@section('content')

<div class="container ml-30">
          <div class="row mb-5 justify-content-center">
            <div class="col-md-8 text-center">
              <h2 class="section-title mb-3" data-aos="fade-up" data-aos-delay="">Our Team</h2>
              <p class="lead" data-aos="fade-up" data-aos-delay="100">Tentang Kami</p>
            </div>
          </div>
          <div class="row">
            
  
            <div class="col-md-6 col-lg-3 mb-4" data-aos="fade-up" data-aos-delay="">
              <div class="team-member">
                <figure>
                  <ul class="social">
                    <li><a href="http://facebook.com"><span class="icon-facebook"></span></a></li>
                    <li><a href="http://twitter.com"><span class="icon-twitter"></span></a></li>
                    <li><a href="http://linkedin.com"><span class="icon-linkedin"></span></a></li>
                    <li><a href="http://instagram.com/arifurahman.br"><span class="icon-instagram"></span></a></li>
                  </ul>
                  <img src="{{ asset('project/images/person_5.jpg')}}" alt="Image" class="img-fluid">
                </figure>
                <div class="p-3">
                  <h3>Arifu Rahman Bastari Rodzikin</h3>
                  <span class="position">Product Manager</span>
                </div>
              </div>
            </div>
  
            <div class="col-md-6 col-lg-3 mb-4" data-aos="fade-up" data-aos-delay="100">
              <div class="team-member">
                <figure>
                  <ul class="social">
                    <li><a href="http://facebook.com"><span class="icon-facebook"></span></a></li>
                    <li><a href="http://twitter.com"><span class="icon-twitter"></span></a></li>
                    <li><a href="http://linkedin.com"><span class="icon-linkedin"></span></a></li>
                    <li><a href="http://instagram.com"><span class="icon-instagram"></span></a></li>
                  </ul>
                  <img src="{{ asset('project/images/person_6.jpg')}}" alt="Image" class="img-fluid">
                </figure>
                <div class="p-3">
                  <h3>Ahmad Naufal Alfakhar</h3>
                  <span class="position">Product Manager</span>
                </div>
              </div>
            </div>
  
            <div class="col-md-6 col-lg-3 mb-4" data-aos="fade-up" data-aos-delay="200">
              <div class="team-member">
                <figure>
                  <ul class="social">
                    <li><a href="http://facebook.com"><span class="icon-facebook"></span></a></li>
                    <li><a href="http://twitter.com"><span class="icon-twitter"></span></a></li>
                    <li><a href="http://facebook.com"><span class="icon-linkedin"></span></a></li>
                    <li><a href="http://instagram.com"><span class="icon-instagram"></span></a></li>
                  </ul>
                  <img src="{{ asset('project/images/person_7.jpg')}}" alt="Image" class="img-fluid">
                </figure>
                <div class="p-3">
                  <h3>Nabila Suraya</h3>
                  <span class="position">Product Manager</span>
                </div>
              </div>
            </div>
  
            
  
@endsection