<!doctype html>
<html lang="en">
  <head>
    <title>Nitro </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="{{ asset('project/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('project/css/jquery-ui.css')}}">
    <link rel="stylesheet" href="{{ asset('project/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{ asset('project/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{ asset('project/css/owl.theme.default.min.css')}}">

    <link rel="stylesheet" href="{{ asset('project/css/jquery.fancybox.min.css')}}">

    <link rel="stylesheet" href="{{ asset('project/css/bootstrap-datepicker.css')}}">

    <link rel="stylesheet" href="{{ asset('project/fonts/flaticon/font/flaticon.css')}}">

    <link rel="stylesheet" href="{{ asset('project/css/aos.css')}}">

    <link rel="stylesheet" href="{{ asset('project/css/style.css')}}">

    
  </head>
  

  <div id="overlayer"></div>
  <div class="loader">
    <div class="spinner-border text-primary" role="status">
      <span class="sr-only">Loading...</span>
    </div>
  </div>

  <div class="container-fluid">
  
 
    <!-- Content Wrapper. Contains page content -->
    <div class="wrapper">
      <!-- Content Header (Page header) -->
      
  
      <!-- Main content -->
      
      @yield('title')
        @yield('content')
       
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
  
    
  </div>
  

  </div> <!-- .site-wrap -->

  <script src="{{ asset('/project/js/jquery-3.3.1.min.js')}}"></script>
  <script src="{{ asset('project/js/jquery-ui.js')}}"></script>
  <script src="{{ asset('project/js/popper.min.js')}}"></script>
  <script src="{{ asset('project/js/bootstrap.min.js')}}"></script>
  <script src="{{ asset('project/js/owl.carousel.min.js')}}"></script>
  <script src="{{ asset('project/js/jquery.countdown.min.js')}}"></script>
  <script src="{{ asset('project/js/jquery.easing.1.3.js')}}"></script>
  <script src="{{ asset('project/js/aos.js')}}"></script>
  <script src="{{ asset('project/js/jquery.fancybox.min.js')}}"></script>
  <script src="{{ asset('project/js/jquery.sticky.js')}}"></script>
  <script src="{{ asset('project/js/isotope.pkgd.min.js')}}"></script>

  
  <script src="{{ asset('project/js/main.js')}}"></script>
    
  </body>
</html>